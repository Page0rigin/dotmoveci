using UnityEngine;

public class Cursor : MonoBehaviour
{
    [SerializeField] SpriteRenderer cursorSprite;
    [SerializeField] Camera cameraMain;

    // Update is called once per frame
    void Update()
    {
        cursorSprite.transform.position = cameraMain.ScreenToWorldPoint(Input.mousePosition);
        cursorSprite.transform.position += new Vector3(0, 0, 10);
	}
}
